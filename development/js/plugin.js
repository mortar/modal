(function( $ ) {
 
    $.fn.modal = function(options) {

        var defaults = {   
            debug:      false,
            speed:      500
        };
     
        var settings = $.extend( {}, defaults, options );

        var getSpeed = function(obj) {
            var speed = settings.speed;
            if(obj.data('transition-speed')) {
                speed = obj.data('transition-speed');
            }
            return speed;
        }

        var openModal = function(obj) {
            var speed = getSpeed(obj);

            var width = $(obj).width();
            var height = $(obj).height();

            if(width > $(window).width()) {
                width = ($(window).width() - 30);
                height = 0;
            }

            if(height > $(window).height()) {
                width = 0;
                height = ($(window).height() - 30);
            }

            $(obj).css({
                'width':        ((width == 0) ? 'auto' : width + 'px'),
                'height':       ((height == 0) ? 'auto' : height + 'px')
            });

            $(obj).css({
                'margin-left':  '-' + ($(obj).width() / 2) + 'px',
                'margin-top':   '-' + ($(obj).height() / 2) + 'px'
            });

            $('.m-modal-overlay').fadeIn(speed);
            $(obj).fadeIn(speed);
        };

        var closeModal = function(obj) {
            var speed = getSpeed(obj);
            $(obj).fadeOut(speed);
            $('.m-modal-overlay').fadeOut(speed);
        }

        this.each(function () {
            $('[data-toggle="modal"]').off().on('click', function(e) {
                e.preventDefault();
                openModal( $($(this).data('target')) );
            });

            $('[data-hide="modal"], .m-modal-overlay').off().on('click', function(e) {
                e.preventDefault();
                closeModal( $('.m-modal') );
            });
        });

        return this;
    };

}( jQuery ));